/**
 * @keep_warm
 * @compute_size 1core_512mb
 * @topology_group api
 */

 import * as express from 'express';
 import uuidAPI = require('uuid');
 
 const uuid = uuidAPI.v4
 const { router, app } = setupExpressApp();
 
 /**
 * Persists the native Javascript Map when compiled
 * @capability kv_persist eventually_consistent
 */
 let quoteStore = new Map<string, string>([["options", {
   mapId: "quoteKV",
   batchWrite: false,
   writeOnChange: false
 } as any]]);
 
 quoteStore.delete("options")
 
 router.post('/v1/quote', async (req, res) => {
   try {
     await quoteStore.set(req.body.quote, req.body.quote)
     res.send(`Added ${req.body.quote}`)
   } catch (err) {
     res.status(500).send({
       err: err,
       msg: "Error Happened"
     })
   }
 
 });
 
 router.get('/v1/quote-list', async (req, res) => {
   let quoteList = Array.from(await quoteStore.entries()).map(x => x[1])
   res.send(quoteList);
 });
 
 function setupExpressApp() {
   const app: any = express();
   const router = express.Router();
   let cors = require('cors');
   router.use(cors());
   router.use(express.urlencoded({ extended: true }));
   router.use(express.json());
   return { router, app };
 }
 
 if (process.env["CLOUDCC"] != "true") {
 
   app.listen(3000, async () => {
     console.log(`App listening locally`)
   })
 }
 
 app.use('/', router)
 
 /**
  * Connects the webapp to the Internet with an API Gateway
  * @capability https_server
  */
 exports.app = app